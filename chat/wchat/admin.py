from django.contrib import admin

from .models import UserChat, Conversation, Comment

admin.site.register(UserChat)
admin.site.register(Conversation)
admin.site.register(Comment)
