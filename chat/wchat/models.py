from django.db import models

class UserChat(models.Model):
    username = models.CharField(max_length=20, help_text='identify each user')
    job = models.CharField(max_length=40, help_text='job of each user', default="Sin trabajo")

    def get_conversation_with_user(self, user):
        conversation = Conversation.objects.filter(creator=self, receiver=user)
        if not conversation.exists():
            conversation = Conversation.objects.filter(creator=user, receiver=self)
        if conversation.exists():
            return conversation[0]
        else:
            None

    def __unicode__(self):
        return u'Username: %s' % self.username

    class Meta:
        ordering = ['username']

class Conversation(models.Model):
    name = models.CharField(max_length=20, help_text='identify each conversation', default="Conversation")
    creator = models.ForeignKey('UserChat', related_name="created_chats", help_text='user who created the chat')
    receiver = models.ForeignKey('UserChat', related_name="received_chats", help_text='user who received the chat')
    timestamp = models.DateTimeField(auto_now=True, help_text='date of creation of comment')

    def __unicode__(self):
        return self.name

    class Meta:
        get_latest_by = 'timestamp'
        ordering = ['timestamp']

class Comment(models.Model):
    text = models.CharField(max_length=700, help_text='content of comment')
    creator = models.ForeignKey('UserChat', related_name="comments", help_text='user who created the comment')
    conversation = models.ForeignKey('Conversation', related_name="comments", help_text='conversation where comment were created')
    creationDate = models.DateTimeField(auto_now_add=True, help_text='date of creation of comment')

    def __unicode__(self):
        return self.text

    class Meta:
        get_latest_by = 'creationDate'
        ordering = ['creationDate']
