from django.core.management.base import BaseCommand, CommandError
from wchat.models import UserChat

class Command(BaseCommand):
    args = 'create users: user1 user2 user3...'
    help = 'Create users with the given usernames one by one'

    def handle(self, *args, **options):
        if len(args) < 1:
            raise CommandError(self.usage('create_users'))
        try:
            for username in args:
                    user = UserChat.objects.filter(username=username)
                    if user.exists():
                        self.stderr.write("An user already exists with this username: %s" % (username))
                    else:
                        user = UserChat(username=username)
                        user.save()
                        self.stderr.write("Create user with username: %s" % (username))
            self.stderr.write("Finished creation...")
        except Exception as err:
            raise CommandError('Error in creation of users %s' % err)

