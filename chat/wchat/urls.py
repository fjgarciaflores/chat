from django.conf.urls import url

import wchat.views as v

urlpatterns = [
    url(r'^$', v.index, name='index'),
    url(r'^(?P<user_id>[A-Za-z0-9_\-]+)/$', v.users, name='users'),
    url(r'^(?P<user_id>[A-Za-z0-9_\-]+)/(?P<conversation_id>[A-Za-z0-9_\-]+)/$', v.comments, name='comments'),
]