# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.CharField(help_text=b'content of comment', max_length=700)),
                ('creationDate', models.DateTimeField(help_text=b'date of creation of comment', auto_now_add=True)),
            ],
            options={
                'ordering': ['creationDate'],
                'get_latest_by': 'creationDate',
            },
        ),
        migrations.CreateModel(
            name='Conversation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'Conversation', help_text=b'identify each conversation', max_length=20)),
                ('timestamp', models.DateTimeField(help_text=b'date of creation of comment', auto_now=True)),
            ],
            options={
                'ordering': ['timestamp'],
                'get_latest_by': 'timestamp',
            },
        ),
        migrations.CreateModel(
            name='UserChat',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('username', models.CharField(help_text=b'identify each user', max_length=20)),
            ],
            options={
                'ordering': ['username'],
            },
        ),
        migrations.AddField(
            model_name='conversation',
            name='creator',
            field=models.ForeignKey(related_name='created_chats', to='wchat.UserChat', help_text=b'user who created the chat'),
        ),
        migrations.AddField(
            model_name='conversation',
            name='receiver',
            field=models.ForeignKey(related_name='received_chats', to='wchat.UserChat', help_text=b'user who received the chat'),
        ),
        migrations.AddField(
            model_name='comment',
            name='conversation',
            field=models.ForeignKey(related_name='comments', to='wchat.Conversation', help_text=b'conversation where comment were created'),
        ),
        migrations.AddField(
            model_name='comment',
            name='creator',
            field=models.ForeignKey(related_name='comments', to='wchat.UserChat', help_text=b'user who created the comment'),
        ),
    ]
