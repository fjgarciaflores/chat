# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wchat', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='userchat',
            name='job',
            field=models.CharField(default=b'Sin trabajo', help_text=b'job of each user', max_length=40),
        ),
    ]
