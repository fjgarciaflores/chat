from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse
from wchat.models import UserChat, Conversation, Comment
from django.views.decorators.csrf import csrf_protect
from django.template import RequestContext
from django.shortcuts import render_to_response

def index(request):
    '''Render the given template with list of users'''
    users = UserChat.objects.all()
    return render(request, 'index.html', {'users': users})

def users(request, user_id):
    '''Render the given template with list of conversations'''
    user = get_object_or_404(UserChat, id=user_id)
    opened_chats = []
    closed_chats = []
    for us in UserChat.objects.exclude(id=user_id):
    	chat = user.get_conversation_with_user(us)
    	if chat:
    		if chat.comments.all():
	    		opened_chats.append({'conversation': chat.id, 'user': us})
	    	else:
	    		closed_chats.append({'conversation': chat.id, 'user': us})
    	else:
    		name = "Conversacion: %s - %s" % (user.username, us.username)
    		new_chat = Conversation(name=name)
    		new_chat.creator = user
    		new_chat.receiver = us
    		new_chat.save()
    		closed_chats.append({'conversation': new_chat.id, 'user': us})

    return render(request, 'user.html', {'user': user, 'opened_chats': opened_chats, 'closed_chats': closed_chats})

@csrf_protect
def comments(request, user_id, conversation_id):
    context = RequestContext(request)
    user = get_object_or_404(UserChat, id=user_id)
    conversation = get_object_or_404(Conversation, id=conversation_id)
    if request.method == "POST":
    	comment = request.POST.get('comment', '')
    	if comment:
	    	new_comment = Comment(text=comment)
	    	new_comment.creator = user
	    	new_comment.conversation = conversation
	    	new_comment.save()
        return render_to_response('chat.html', {'chat': conversation, 'user': user}, context)
    else:
        return render_to_response('chat.html', {'chat': conversation, 'user': user}, context)
