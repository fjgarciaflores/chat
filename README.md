Este proyecto incluye:

	Un chat para usuarios que pueden ser creados desde el panel de administrador o haciendo
		uso del comando "create_users".

Ha sido desarrollado con:

	Python version:
		2.7.6

	Django version:
		1.8.12 

Los pasos seguidos en el desarrollo:
1) Crear repositorio

2) Crear proyecto de Django

3) Crear app de Django

4) Configurar base de datos y crear tablas
	
5) Crear modelo: comentario, usuario.

	*UserChat:

		- id

		- username

		- job

	*Conversation:

		- id

		- creator

		- receiver

		- timestamp

	*Comment:

		- id

		- text

		- creator

		- conversation

		- creationDate

5.1) Crear usuarios y comentarios de ejemplo.

	- Habilitar Admin panel

	- Crear "superuser" para trabajar en panel de administración.

	- Crear comando para añadir usuarios desde la terminal.
		
6) Crear url y vistas 

	inicial:

		-localhost/

	usuario:

		-localhost/<user_id>/

	chat:

		-localhost/<user_id>/<conversation_id>

8) Crear templates

	- Block general

	- inicial (lista de usuarios)

	- usuario (lista de chats)

	- chat (presionar tecla "Enter" o click en botón para introducir comentario, alineados con nombre y fecha)

9) Añadir nuevas migraciones como material complementario.